


( () => {



    const MainCtrl = ( $scope ) => {



        const setup = () => {
            console.log( '$scope (MainCtrl): ', $scope )
        }
        setup()
    }



    angular.module( 'webapp.public' )
        .controller( 'webapp.public.ctrl.main', MainCtrl )
} )()
