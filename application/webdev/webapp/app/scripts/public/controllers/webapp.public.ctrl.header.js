


( () => {



    const HeaderCtrl = ( $scope ) => {



        const setup_environment = () => {
            $scope.menu = {}
            $scope.menu.show = false
        }



        $scope.CloseMenu = () => {
            $scope.menu.show = false
        }



        const setup = () => {
            console.log( '$scope ( HeaderCtrl ): ', $scope )
            setup_environment()
        }

        setup()
    }



    angular.module( 'webapp.public' )
        .controller( 'webapp.public.ctrl.header', HeaderCtrl )
} )()
