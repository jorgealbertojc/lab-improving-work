


( () => {



    module.exports = ( _grunt ) => {



        var api = {
            name: 'html2js',
            config: {}
        }



        _grunt.webdev.apps.forEach( ( _app ) => {
            api.config[ _app + '-views-app' ] = {
                options: {
                    base: _grunt.webdev.dirs.base + '/',
                    indentString: '    ',
                    module: 'webapp.' + _app + '.views.tpls',
                    quoteChar: '\'',
                    singleModule: true,
                    useStrict: true,
                    htmlmin: {
                        collapseWhitespace: true,
                        removeComments: true,
                        removeScriptTypeAttributes: true,
                        removeStyleLinkTypeAttributes: true,
                    }
                },
                src: [ _grunt.webdev.dirs.dest + '/views/' + _app + '/**/*.html' ],
                dest: _grunt.webdev.dirs.dest + '/scripts/' + _app + '/webapp/webapp.' + _app + '.views.tpls.js',
            }

            api.config[ _app + '-views-directives' ] = {
                options: {
                    base: _grunt.webdev.dirs.base + '/scripts/' + _app + '/',
                    indentString: '    ',
                    module: 'webapp.' + _app + '.drtv.tpls',
                    quoteChar: '\'',
                    singleModule: true,
                    useStrict: true,
                    htmlmin: {
                        collapseWhitespace: true,
                        removeComments: true,
                        removeScriptTypeAttributes: true,
                        removeStyleLinkTypeAttributes: true,
                    }
                },
                src: [ _grunt.webdev.dirs.dest + '/scripts/' + _app + '/directives/**/*.html' ],
                dest: _grunt.webdev.dirs.dest + '/scripts/' + _app + '/webapp/webapp.' + _app + '.drtv.tpls.js',
            }
        } )



        return api
    }
} )()
