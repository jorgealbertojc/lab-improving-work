


( () => {



    module.exports = ( _grunt ) => {



        var api = {
            name: 'postcss',
            config: {}
        }



        api.config.options = {
            processors: [
                require( 'autoprefixer' )( { browsers: [ 'last 200 version' ] } )
            ]
        }



        _grunt.webdev.apps.forEach( ( _app ) => {
            //- app styles
            api.config[ _app + '-styles-app' ] = {
                files: [
                    {
                        expand: true,
                        cwd: _grunt.webdev.dirs.dest + '/styles/' + _app,
                        src: [
                            '**/*.css',
                            '!**/*.min.css'
                        ],
                        dest: _grunt.webdev.dirs.dest + '/styles/' + _app,
                        extDot: 'last',
                    },
                ]
            }

            //- app directives styles
            api.config[ _app + '-styles-directives' ] = {
                files: [
                    {
                        expand: true,
                        cwd: _grunt.webdev.dirs.dest + '/scripts/' + _app + '/directives',
                        src: [
                            '**/*.css',
                            '!**/*.min.css'
                        ],
                        dest: _grunt.webdev.dirs.dest + '/scripts/' + _app + '/directives',
                        extDot: 'last',
                    }
                ]
            }
        } )



        return api
    }
} )()
