


( () => {



    module.exports = ( _grunt ) => {



        var api = {
            name: 'cssmin',
            config: {}
        }



        _grunt.webdev.apps.forEach( ( _app ) => {
            api.config[ _app ] = {
                files: [

                    //- app styles
                    {
                        expand: true,
                        cwd: _grunt.webdev.dirs.dest + '/styles/' + _app,
                        src: [
                            '**/*.css',
                            '!**/*.min.css',
                        ],
                        dest: _grunt.webdev.dirs.dest + '/styles/' + _app,
                        estDot: 'last',
                        ext: '.min.css',
                    },

                    //- app directives styles
                    {
                        expand: true,
                        cwd: _grunt.webdev.dirs.dest + '/scripts/' + _app + '/directives',
                        src: [
                            '**/*.css',
                            '!**/*.min.css',
                        ],
                        dest: _grunt.webdev.dirs.dest + '/scripts/' + _app + '/directives',
                        estDot: 'last',
                        ext: '.min.css',
                    }
                ]
            }
        } )



        return api
    }
} )()
