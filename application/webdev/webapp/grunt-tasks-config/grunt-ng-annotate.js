


( () => {



    module.exports = ( _grunt ) => {



        var api = {
            name: 'ngAnnotate',
            config: {}
        }



        _grunt.webdev.apps.forEach( ( _app ) => {
            api.config[ _app ] = {
                options: {
                    singleQuotes: true,
                },
                files: [
                    {
                        expand: true,
                        cwd: _grunt.webdev.dirs.dest + '/scripts/' + _app,
                        src: [
                            '**/*.js',
                            '!**/*.min.js'
                        ],
                        dest: _grunt.webdev.dirs.dest + '/scripts/' + _app,
                    }
                ]
            }
        } )



        return api
    }
} )()
