


( () => {



    module.exports = ( _grunt ) => {



        var api = {
            name: 'babel',
            config: {}
        }



        api.config.options = {
            sourceMap: false,
            presets: [ 'es2015' ]
        }



        _grunt.webdev.apps.forEach( ( _app ) => {
            api.config[ _app ] = {
                files: [
                    {
                        expand: true,
                        cwd: _grunt.webdev.dirs.src + '/scripts/' + _app,
                        src: [
                            '**/*.js',
                            '!**/*.min.js',
                        ],
                        dest: _grunt.webdev.dirs.dest + '/scripts/' + _app,
                        extDot: 'last',
                    }
                ]
            }
        } )



        return api
    }
} )()
