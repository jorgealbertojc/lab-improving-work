


( () => {



    module.exports = ( _grunt ) => {



        var api = {
            name: 'tinypng',
            config: {}
        }



        api.config.options = {
            apiKey: _grunt.webdev.keys.tinypng,
            showProgress: false,
            stopOnImageError: false,
        }



        _grunt.webdev.apps.forEach( ( _app ) => {
            api.config[ _app ] = {
                files: [
                    {
                        expand: true,
                        cwd: _grunt.webdev.dirs.dest + '/images/' + _app,
                        src: [
                            '**/*.{jpg,jpeg,png}',
                        ],
                        dest: _grunt.webdev.dirs.dest + '/images/' + _app,
                        extDot: 'last',
                    }
                ]
            }
        } )



        return api
    }
} )()
