


( () => {



    module.exports = ( _grunt ) => {



        var api = {
            name: 'pug',
            config: {}
        }



        _grunt.webdev.apps.forEach( ( _app ) => {
            //- views
            api.config[ _app + '-views-app' ] = {
                files: [
                    {
                        expand: true,
                        cwd: _grunt.webdev.dirs.src + '/views/' + _app,
                        src: [
                            '**/*.pug',
                            '!**/components/**',
                            '!**/layouts/**',
                            '!**/mixins/**',
                        ],
                        dest: _grunt.webdev.dirs.dest + '/views/' + _app,
                        extDot: 'last',
                        ext: '.html',
                    },
                ]
            }

            //- directives
            api.config[ _app + '-views-directives' ] = {
                files: [
                    {
                        expand: true,
                        cwd: _grunt.webdev.dirs.src + '/scripts/' + _app + '/directives',
                        src: [
                            '**/*.pug',
                            '!**/components/**',
                            '!**/layouts/**',
                            '!**/mixins/**',
                        ],
                        dest: _grunt.webdev.dirs.dest + '/scripts/' + _app + '/directives',
                        extDot: 'last',
                        ext: '.html',
                    }
                ]
            }



            //- php views
            api.config[ _app + '-views-php' ] = {
                files: [
                    {
                        expand: true,
                        cwd: _grunt.webdev.dirs.src + '/php/' + _app,
                        src: [
                            '**/*.pug',
                            '!**/components/**',
                            '!**/layouts/**',
                            '!**/mixins/**',
                        ],
                        dest: _grunt.webdev.dirs.dest + '/application/views/' + _app,
                        extDot: 'last',
                        ext: '.php'
                    }
                ]
            }
        } )



        return api
    }
} )()
