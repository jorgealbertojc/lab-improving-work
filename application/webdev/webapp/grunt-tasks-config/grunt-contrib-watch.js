


( () => {



    module.exports = ( _grunt ) => {



        var api = {
            name: 'watch',
            config: {}
        }



        _grunt.webdev.apps.forEach ( ( _app ) => {



            api.config[ _app + '-styles-app' ] = {
                files: [
                    _grunt.webdev.dirs.src + '/styles/' + _app + '/**/*.styl'
                ],
                tasks: [
                    'stylus:' + _app + '-styles-app',
                    'postcss:' + _app + '-styles-app'
                ]
            }



            api.config[ _app + '-styles-directives' ] = {
                files: [
                    _grunt.webdev.dirs.src + '/scripts/' + _app + '/directives/**/*.styl'
                ],
                tasks: [
                    'stylus:' + _app + '-styles-directives',
                    'postcss:' + _app + '-styles-directives',
                ]
            }



            api.config[ _app + '-scripts' ] = {
                files: [
                    _grunt.webdev.dirs.src + '/scripts/' + _app + '/**/*.js'
                ],
                tasks: [
                    'newer:babel:' + _app,
                    'ngAnnotate:' + _app,
                ]
            }



            api.config[ _app + '-views-app' ] = {
                files: [
                    _grunt.webdev.dirs.src + '/views/' + _app + '/**/*.pug'
                ],
                tasks: [
                    'newer:pug:' + _app + '-views-app',
                    'html2js:' + _app + '-views-app',
                ]
            }



            api.config[ _app + '-views-directives' ] = {
                files: [
                    _grunt.webdev.dirs.src + '/scripts/' + _app + '/directives/**/*.pug'
                ],
                tasks: [
                    'newer:pug:' + _app + '-views-directives',
                    'html2js:' + _app + '-views-directives',
                ]
            }



            api.config[ _app + '-views-php' ] = {
                files: [
                    _grunt.webdev.dirs.src + '/php/' + _app + '/**/*.pug'
                ],
                tasks: [
                    'newer:pug:' + _app + '-views-php',
                ]
            }
        } )



        return api
    }
} )()
