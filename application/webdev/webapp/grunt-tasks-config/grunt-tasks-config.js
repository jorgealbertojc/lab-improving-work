


( () => {



    module.exports = ( _grunt ) => {



        var fs = require( 'fs' )
        var list_files = fs.readdirSync( _grunt.webdev.dirs.tasks )
        var filtered_list_files = []
        var grunt_init_config = {}



        const setup = () => {
            setup_include_list()
            include_grunt_plugin_settings()
        }



        const get_file_ext = ( _filename ) => _filename.substring( _filename.length - 3 )



        const get_filename_without_ext = ( _filename ) => _filename.substring( 0, _filename.length - 3 )



        const include_grunt_plugin_settings = () => {
            var task = null
            var task_config = null
            filtered_list_files.forEach( ( _filename ) => {
                task = require( './' + get_filename_without_ext( _filename ) )
                if ( typeof task === 'function' ) {
                    task_config = task( _grunt )
                    if ( task_config &&
                    task_config.name &&
                    typeof task_config.name === 'string' &&
                    task_config.config &&
                    typeof task_config.config === 'object' ) {
                        grunt_init_config[ task_config.name ] = task_config.config
                    }
                }
            } )
            _grunt.initConfig( grunt_init_config )
        }



        const setup_include_list = () => {
            var filename = null
            var file_ext = null



            list_files.forEach( ( _file ) => {
                filename = _file
                file_ext = get_file_ext( filename )
                if ( file_ext === '.js' && filename !== 'grunt-tasks-config.js' ) {
                    filtered_list_files.push( filename )
                }
            } )
        }



        setup()
    }
} )()
