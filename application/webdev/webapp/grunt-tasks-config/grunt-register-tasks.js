


( () => {



    module.exports = ( _grunt ) => {



        _grunt.registerTask( 'default', 'The default task',  ( _target ) => {
            console.log( '' )
            console.log( 'BIENVENIDO'.bold )
            console.log( '' )
        } )



        _grunt.webdev.apps.forEach( ( _app ) => {
            _grunt.registerTask( 'build-' + _app, [
                'clean:' + _app,
                'copy:' + _app,

                'clean:components',
                'copy:components',

                'stylus:' + _app + '-styles-app',
                'postcss:' + _app + '-styles-app',
                'stylus:' + _app + '-styles-directives',
                'postcss:' + _app + '-styles-directives',

                'babel:' + _app,
                'ngAnnotate:' + _app,

                'pug:' + _app + '-views-app',
                'html2js:' + _app + '-views-app',
                'pug:' + _app + '-views-directives',
                'html2js:' + _app + '-views-directives',

                'pug:' + _app + '-views-php',
            ] )
        } )



        _grunt.registerTask( 'dev',
            'Grunt dev tasks',
            ( _target ) => {

                if ( ! _target ) {
                    _grunt.tast.run( 'default' );
                    return;
                }

                _grunt.task.run( [
                    'clean:' + _target,
                    'copy:' + _target,

                    'clean:components',
                    'copy:components',

                    'stylus:' + _target + '-styles-app',
                    'postcss:' + _target + '-styles-app',
                    'stylus:' + _target + '-styles-directives',
                    'postcss:' + _target + '-styles-directives',

                    'babel:' + _target,
                    'ngAnnotate:' + _target,

                    'pug:' + _target + '-views-app',
                    'html2js:' + _target + '-views-app',
                    'pug:' + _target + '-views-directives',
                    'html2js:' + _target + '-views-directives',

                    'pug:' + _target + '-views-php',
                    'watch',
                ] )
            }
        )



        _grunt.registerTask( 'make', () => {
            _grunt.webdev.apps.forEach( ( _app ) => {
                _grunt.task.run( 'build-' + _app )
            } )
        } )



        return null
    }
} )()
