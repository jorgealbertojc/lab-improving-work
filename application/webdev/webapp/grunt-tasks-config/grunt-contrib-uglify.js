


( () => {



    module.exports = ( _grunt ) => {



        var api = {
            name: 'uglify',
            config: {}
        }



        _grunt.webdev.apps.forEach( ( _app ) => {
            api.config[ _app ] = {
                files: [
                    {
                        expand: true,
                        cwd: _grunt.webdev.dirs.dest + '/scripts/' + _app,
                        src: [
                            '**/*.js',
                            '!**/*.min.js',
                        ],
                        dest: _grunt.webdev.dirs.dest + '/scripts/' + _app,
                        extDot: 'last',
                        ext: '.min.js',
                    }
                ]
            }
        } )



        return api
    }
} )()
