


( () => {



    module.exports = ( _grunt ) => {



        var api = {
            name: 'clean',
            config: {}
        }



        api.config.options = {
            force: true
        };



        api.config.components = {
            src: [
                _grunt.webdev.dirs.dest + '/scripts/components',
                _grunt.webdev.dirs.dest + '/styles/components',
            ]
        }



        //- beg: scripts & styles
        _grunt.webdev.apps.forEach( ( _app ) => {
            api.config[ _app ] = [
                _grunt.webdev.dirs.dest + '/scripts/' + _app,
                _grunt.webdev.dirs.dest + '/styles/' + _app,
                _grunt.webdev.dirs.dest + '/application/views/' + _app,
            ]
        } )
        //- end: scripts & styles



        return api
    }
} )()
