


( () => {



    module.exports = ( _grunt ) => {



        var api = {
            name: 'stylus',
            config: {}
        }



        _grunt.webdev.apps.forEach( ( _app ) => {
            //- app styles
            api.config[ _app + '-styles-app' ] = {
                files: [
                    {
                        expand: true,
                        cwd: _grunt.webdev.dirs.src + '/styles/' + _app,
                        src: [
                            '**/*.styl',
                            '!**/_*.styl'
                        ],
                        dest: _grunt.webdev.dirs.dest + '/styles/' + _app,
                        extDot: 'last',
                        ext: '.css',
                    },
                ]
            }

            //- app directives styles
            api.config[ _app + '-styles-directives' ] = {
                files: [
                    {
                        expand: true,
                        cwd: _grunt.webdev.dirs.src + '/scripts/' + _app + '/directives',
                        src: [
                            '**/*.styl',
                            '!**/_*.styl'
                        ],
                        dest: _grunt.webdev.dirs.dest + '/scripts/' + _app + '/directives',
                        extDot: 'last',
                        ext: '.css',
                    }
                ]
            }
        } )



        return api
    }
} )()
