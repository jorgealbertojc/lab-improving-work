


( () => {



    module.exports = ( _grunt ) => {



        var api = {
            name: 'copy',
            config: {}
        }



        api.config.components = {
            files: [

                // scripts
                {
                    expand: true,
                    cwd: _grunt.webdev.dirs.src + '/components',
                    src: [
                        '**/*.js'
                    ],
                    dest: _grunt.webdev.dirs.dest + '/scripts/components',
                    extDot: 'last',
                },

                // styles && images
                {
                    expand: true,
                    cwd: _grunt.webdev.dirs.src + '/components',
                    src: [
                        '**/*.css',
                        '**/*.{jpg,jpeg,png,ico,gif}',
                        '**/*.{svg,woff,woff2,eot,ttf}'
                    ],
                    dest: _grunt.webdev.dirs.dest + '/styles/components',
                    extDot: 'last',
                }
            ]
        }



        //- beg: images
        _grunt.webdev.apps.forEach( ( _app ) => {
            api.config[ _app ] = {
                files: [

                    // images
                    {
                        expand: true,
                        cwd: _grunt.webdev.dirs.src + '/images/' + _app,
                        src: [
                            '**/*.{jpg,jpeg,png,ico,gif,svg}',
                        ],
                        dest: _grunt.webdev.dirs.dest + '/images/' + _app,
                        extDot: 'last',
                    },

                    // fonts
                    {
                        expand: true,
                        cwd: _grunt.webdev.dirs.src + '/fonts/' + _app,
                        src: [
                            '**/*.{svg,woff,woff2,eot,ttf}',
                        ],
                        dest: _grunt.webdev.dirs.dest + '/fonts/' + _app,
                        extDot: 'last',
                    }
                ]
            }
        } )
        //- end: images



        return api
    }
} )()
