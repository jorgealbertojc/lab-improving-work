


( () => {



    module.exports = ( _grunt ) => {



        require('shelljs/global')


        //-
        //- El parametro de versión puede tomar los siguientes valores
        //- * patch
        //- * feature
        //- * release
        const update_version = ( _target ) => {
            _grunt.task.run( 'make', 'version-builder:' + _target + '' )
        }



        const create_tag = ( _tag_name ) => {
            console.log( '' )
            console.log( '' )
            console.log( '[ INFO ] - ', 'Tagging as v' + _tag_name )
            console.log( '' )

            if ( exec( 'git tag -a v' + _tag_name + ' -m "version v' + _tag_name + '"' ).code !== 0 ) {
                console.log( '[ ERROR ] - '.red, 'an error has ocurred during tagging' )
                exit( 1 )
                return
            } else {
                console.log( '[ SUCCESS ] - '.green, 'tag v' + _tag_name + ' was created successfully' )
                console.log( '              please push manually to your repo.' )
            }

            build_changelog( _tag_name )
        }



        const build_changelog = ( _tag_name ) => {
            var log_file_contents = '# CHANGELOG v' + _tag_name + ': \n\n'
            log_file_contents += '## FEATURES: \n\n'

            console.log()
            console.log()
            console.log( 'NEW FEATURES: '.bold.green )
            console.log()

            var log_features = exec( 'git log -g --grep="feat" --format="> %B" -n 10' )

            log_file_contents += log_features

            log_file_contents += '\n\n## FIXES: \n\n'

            console.log()
            console.log()
            console.log( 'FIXED THINGS: '.bold.green )
            console.log()

            var log_fixes = exec( 'git log -g --grep="fix" --format="> %B" -n 10' )

            log_file_contents += log_fixes

            _grunt.file.write( _grunt.webdev.dirs.dest + '/CHANGELOG.md', log_file_contents )

            console.log()
            console.log()
            console.log( 'SUCCESS'.bold.green )
            console.log()
            console.log( 'The version v' + _tag_name + ' was created successfully and the' )
            console.log( 'CHANGELOG.md has been updated.' )
        }



        _grunt.registerTask( 'version-builder', 'Builder for version upgrades', ( _target ) => {
            var level_to_upgrade = 0
            var package_config = null
            var version_levels = null
            var package_filepath = _grunt.webdev.dirs.dest + '/package.json'

            if ( _target === 'release' ) {
                level_to_upgrade = 0
            } else if ( _target === 'feature' ) {
                level_to_upgrade = 1
            } else if ( _target === 'patch' ) {
                level_to_upgrade = 2
            } else {
                console.log('versionning')
                return undefined
            }

            package_config = _grunt.file.readJSON( package_filepath )
            version_levels = package_config.version.split( '.' )

            if ( level_to_upgrade === 0 ) {
                version_levels[ 1 ] = 0
                version_levels[ 2 ] = 0
            }

            if ( level_to_upgrade === 1 ) {
                version_levels[ 2 ] = 0
            }
            version_levels[ level_to_upgrade ] = parseInt( version_levels[ level_to_upgrade ] ) + 1

            console.log( '' )
            console.log( '' )
            console.log( 'Versioning to: '.green.bold, 'v' + version_levels.join( '.' ) )
            console.log( '' )

            package_config.version = version_levels.join( '.' )
            _grunt.file.write( package_filepath, JSON.stringify( package_config, null, 2 ) )

            console.log( '[ SUCCESS ] - '.green, 'versioning it completed successfully' )

            create_tag( version_levels.join( '.' ) )
        } )



        _grunt.registerTask( 'patch', () => {
            update_version( 'patch' )
        } )



        _grunt.registerTask( 'feature', () => {
            update_version( 'feature' )
        } )



        _grunt.registerTask( 'release', () => {
            update_version( 'release' )
        } )



        return null
    }
} )()
