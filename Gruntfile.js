


( () => {



    module.exports = ( grunt ) => {



        grunt.webdev = grunt.file.readJSON( './grunt-config.json' );
        grunt.webdev.keys = grunt.file.readJSON( './.keys' );



        require( 'load-grunt-tasks' )( grunt )
        require( grunt.webdev.dirs.tasks + '/grunt-tasks-config' )( grunt )

        var colors = require( 'colors' );


        if ( ! grunt.webdev.keys.tinypng || ! grunt.webdev.keys.tinypng.length ) {
            console.log( '' )
            console.log( '' )
            console.log( '----------------------------------------'.red )
            console.log( 'ERROR: '.bold.red )
            console.log( '' )
            console.log( 'Please provide a tinypng api key'.red )
            console.log( '----------------------------------------'.red )
            console.log( '' )
            console.log( '' )
            grunt.registerTask( 'default', () => {})
            return;
        }
    }
} )()
