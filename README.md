# lab-improvement-work

Este proyecto servirá de guia para resolver algunas situaciones que nos encontramos todos los dias en nuestro trabajo.

Ayuará a dar una pequeña solución, tal vez no la más optima ni la mejor, pero sí una solución a algunas cosas
que dependen demasiado de la burocracia y que estando dentro de una empresa de desarrollo de software es
lo mínimo que debemos tener.

### Dependencias del proyecto

- PHP 5.6+
- NodeJS 4.X+
- Pug
- GruntJS
- Stylus

### Productos finales

1. Sistema de administración de vacaciones y días de asueto
2. Sistema de administración de currícula ( _ayuda a los reclutadores y entrevistadores_ )
3. Sistema de reservación de salas de reuniones y meetings
4. Sistema de administración de estacionamientos

### Configuraciones especiales

###### grunt-tinypng

Para poder hacer uso de este plugin se debe contar con una api key la cual se puede conseguir en
el sitio web de desarrolladores de tinypng [https://tinypng.com/developers](https://tinypng.com/developers).

Esta api key se debe colocar en el archivo `.keys`, puede encontrar un ejemplo de este archivo en `.keys.sample`
en la raíz del sistema.

Por seguridad de los datos de los desarrolladores no se deben subir estas llaves al repositorio.

### Notas

- Este proyecto se está desarrollando adaptandolo a las situaciones que nosotros conocemos, si tienes alguna situación
especial podrías comentarlo en el [blog](http://www.arkosnoemarenom.com) y contribuir en este repositorio

- Sólo se tomarán como válidos a comentarios constructivos que lleven a mejorar el sistema

- Sólo se harán válidos comentarios que estén en el [blog](http://www.arkosnoemarenom.com) y no se podrá ninguna especial atención a los comentarios
que se agregen al repositorio.

### Compilación y visualización

#### front-end

Grunt es nuestro manejador de tareas, en este se han definido las siguientes tareas

_Para desarrollo_

```bash
# para compilar el área pública
grunt dev:public

# para compilar el área privada
grunt dev:private
```

_Para producción_

> Para ejecutar esta tarea es de suma importancia tener nuestro archivo `.keys` ya configurado con nuestra llave de tinypng

```bash
grunt make
```

Esta tarea al final dejará todo listo para que se pueda hacer el upload a producción.
